{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src='../frameworks/img/EU-Copernicus-EUM-WEKEO_banner_logo.png' align='right' width='75%'></img>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"../Index.ipynb\"><< Index</a>\n",
    "<br>\n",
    "<a href=\"./1_5_OLCI_radiance_reflectance_spectra.ipynb\"><< OLCI radiance and reflectance spectra</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"1_7_OLCI_light_environment.ipynb\">Assessing the light environment >></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color=\"#138D75\">**Copernicus Marine Training Service**</font> <br>\n",
    "**Copyright:** 2022 EUMETSAT <br>\n",
    "**License:** MIT"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<html>\n",
    "  <div style=\"width:100%\">\n",
    "    <div style=\"float:left\"><a href=\"https://trainhub.eumetsat.int/ocean/sensors/1_6_OLCI_CHL_comparison.ipynb\"><img src=\"https://img.shields.io/badge/Launch-TrainHub-Blue.svg\" alt=\"Open in TrainHub\"></a></div>\n",
    "    <div style=\"float:left\"><p>&emsp;</p></div>\n",
    "    <div style=\"float:left\"><a href=\"https://mybinder.org/v2/gl/eumetlab%2Focean%2Fsensors%2Flearn-olci/main?labpath=1_OLCI_introductory/1_6_OLCI_CHL_comparison.ipynb\"><img src=\"https://mybinder.org/badge_logo.svg\" alt=\"Open in Binder\"></a></div>\n",
    "    <div style=\"float:left\"><p>&emsp;</p></div>\n",
    "    <div style=\"float:left\"><a href=\"https://colab.research.google.com/github/eumetlab/ocean/sensors/learn-olci/blob/main/1_OLCI_introductory/1_6_OLCI_CHL_comparison.ipynb\"><img src=\"https://colab.research.google.com/assets/colab-badge.svg\" alt=\"Open In Colab\" /></a></div>\n",
    "    <div style=\"float:left\"><p>&emsp;</p></div>\n",
    "    <div style=\"float:left\"><a href=\"https://studiolab.sagemaker.aws/import/github/eumetlab/ocean/sensors/learn-olci/blob/main/1_OLCI_introductory/1_6_OLCI_CHL_comparison.ipynb\"><img src=\"https://studiolab.sagemaker.aws/studiolab.svg\" alt=\"Open In Studio Lab\"/></a></div>\n",
    "    <div style=\"float:left\"><p>&emsp;</p></div>\n",
    "    <div style=\"float:left\"><a href=\"https://pccompute.westeurope.cloudapp.azure.com/compute/hub/user-redirect/git-pull?repo=https://github.com/eumetlab/ocean/sensors/learn-olci&urlpath=1_OLCI_introductory/1_6_OLCI_CHL_comparison.ipynb&branch=main\"><img src=\"https://img.shields.io/badge/Open-Planetary%20Computer-black?style=flat&logo=microsoft\" alt=\"Open in Planetary Computer\"/></a></div>\n",
    "  </div>\n",
    "</html>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "<h3>Learn OLCI: Introductory</h3></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "    \n",
    "<b>PREREQUISITES </b>\n",
    "    \n",
    "The following modules are prerequisites for this notebook:\n",
    "- **[1_1_OLCI_data_access.ipynb](1_1_OLCI_data_access.ipynb)** (this will download the required OLCI products for this notebook)\n",
    "\n",
    "</div>\n",
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1.6 Comparing OLCI chlorophyll products\n",
    "\n",
    "\n",
    "### Data used\n",
    "\n",
    "| Product Description  | Data Store collection ID|  WEkEO HDA ID | Product Navigator |\n",
    "|:--------------------:|:-----------------------:|:-------------:|:-----------------:|\n",
    "| Sentinel-3 OLCI level-2 full resolution  | EO:EUM:DAT:0407 | EO:EUM:DAT:SENTINEL-3:OL_2_WFR___ | [link](https://navigator.eumetsat.int/product/EO:EUM:DAT:SENTINEL-3:OL_2_WFR___NTC?query=OLCI&filter=satellite__Sentinel-3&filter=instrument__OLCI&filter=processingLevel__Level%202%20Data&s=advanced) |\n",
    "\n",
    "### Learning outcomes\n",
    "\n",
    "At the end of this notebook you will know;\n",
    "* About the different algorithms used to provide estimates of chlorophyll-a from OLCI data\n",
    "* How to plot chlorophyll-a data on a map, with appropriate flags applied\n",
    "\n",
    "\n",
    "### Outline\n",
    "\n",
    "The primary objective of ocean colour satellite missions has historically been to estimate the concentration of chlorophyll-a in ocean waters. Inherent in the function of the chlorophyll-a pigment as it has evolved for photosynthesis, is interaction with sunlight. This makes it readily observable by satellite ocean colour instruments. In the simplest understanding of how chlorophyll-a affects the colour of ocean waters, bluer waters tend to have lower concentrations of chlorophyll-a, whilst greener waters have higher concentrations. This simple relationship underlies many of the algorithms used to estimate chlorophyll-a concentrations from ocean colour data, particularly in the open ocean. However, gaining accurate chlorophyll-a concentrations throughout the global oceans is more complicated in reality, and the relationships between the reflectance spectra and chlorophyll-a concentration vary, depending on the characteristics of the phytoplankton assemblage that is the source of the chlorophyll-a, as well as the relative concentrations of other constituents (sediments, dissolved organic matter etc). \n",
    "\n",
    "In this notebook we'll look at two different algorithm approaches used to generate chlorophyll-a concentrations in OLCI products, and examine where and when it is most appropriate to use each one, and what their limitations are.\n",
    "\n",
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## <a id='TOC_TOP'></a>Contents\n",
    "\n",
    "</div>\n",
    "    \n",
    " 1. [OLCI Chlorophyll products and algorithms](#section1)\n",
    " 2. [Reading OLCI chlorophyll data](#section2)\n",
    " 3. [Understanding and applying flags](#section3)\n",
    " 4. [Plotting and comparing chlorophyll](#section4)\n",
    "\n",
    "<hr>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cartopy.crs as ccrs\n",
    "import eumartools\n",
    "import glob\n",
    "import inspect\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.ticker as mticker\n",
    "import numpy as np\n",
    "import os\n",
    "import warnings\n",
    "import xarray as xr\n",
    "\n",
    "warnings.filterwarnings('ignore')\n",
    "plt.rcParams.update({'font.size': 12})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## <a id='section1'></a>1. OLCI Chlorophyll products and algorithms\n",
    "[Back to top](#TOC_TOP)\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Chlorophyll-a concentrations are provided in the OLCI level-2 products. You can find detailed information about these products in the relevant pages of the [Sentinel-3 knowledge base](https://eumetsatspace.atlassian.net/wiki/spaces/SEN3/pages/1597767909/OLCI+products+level-2).\n",
    "\n",
    "The two products are called:\n",
    "* CHL_OC4ME\n",
    "* CHL_NN\n",
    "\n",
    "The CHL_OC4ME is an algorithm based on an empirical relationship between reflected light measured at several of the blue and green wavebands of OLCI, and in situ measurements of chlorophyll-a concentration. It is applied to reflectances derived from the Baseline Atmospheric Correction (BAC) processing chain. \n",
    "\n",
    "The CHL_NN is an output of the alternative atmospheric correction and comples water processing chain. It involves several neural networks which retrieve the reflectances (not supplied in the standard level-2 OLCI products) and several geophysical products including the chlorophyll-a concentraion, total suspended matter (TSM) concentration, and an estimate of the absorption by coloured dissolved organic matter at detritus at 443 nm. \n",
    "\n",
    "You can find more details on both processing chains in the the OLCI level-2 processing pages of the [Sentinel-3 knowledge base](https://eumetsatspace.atlassian.net/wiki/spaces/SEN3/pages/1597800556/OLCI+processing+level-2). The complex water process (C2RCC) is also available in the [Sentinel-Applications Platform (SNAP)](https://step.esa.int/main/toolboxes/snap/)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## <a id='section2'></a>2. Reading OLCI chlorophyll data\n",
    "[Back to top](#TOC_TOP)\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To begin with, we'll create paths that point to the location of our data of interest."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SAFE_directory = os.path.join(os.getcwd(), 'products',\n",
    "    'S3A_OL_2_WFR____20210717T101015_20210717T101315_20210718T221347_0179_074_122_1980_MAR_O_NT_003.SEN3')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we'll set a parameter to reduce the resolution of the data we are working with. For analyses you would not want to do this, but it will help us to visualise our data much faster in the context of this tutorial notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grid_reduce = 5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we'll open the data we need including the latitude and longitude information, and the two chlorophyll-a products. We'll also calculate the difference between the two chlorphyll-a products."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "geo_file = os.path.join(SAFE_directory,'geo_coordinates.nc')\n",
    "geo_fid = xr.open_dataset(geo_file)\n",
    "lon = geo_fid.longitude.data[::grid_reduce, ::grid_reduce]\n",
    "lat = geo_fid.latitude.data[::grid_reduce, ::grid_reduce]\n",
    "geo_fid.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "band_vars = xr.open_mfdataset(glob.glob(os.path.join(SAFE_directory,'chl*.nc')))\n",
    "band_vars[\"CHL_DIFF\"] = 10**band_vars['CHL_OC4ME'] - 10**band_vars['CHL_NN']\n",
    "band_vars.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## <a id='section3'></a>3. Understanding and applying flags\n",
    "[Back to top](#TOC_TOP)\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As well as the data and the geocoordinates, to plot geophysical products derived from OLCI properly, we also need to extract and apply the relevant quality flags. Below you can see that the flags are extracted from the wqsf netcdf file. If we print out the flags and their associated Bit values, you can get an idea about what they represent, and how to access each individual one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "flag_file = os.path.join(SAFE_directory,'wqsf.nc')\n",
    "flag_vars = xr.open_dataset(flag_file)\n",
    "flag_vars.close()\n",
    "band_vars[\"WQSF\"] = flag_vars[\"WQSF\"]\n",
    "\n",
    "ii = np.argsort(flag_vars[\"WQSF\"].flag_masks)\n",
    "bitvals = np.array(flag_vars[\"WQSF\"].flag_masks)[ii]\n",
    "meanings = np.array(flag_vars[\"WQSF\"].flag_meanings.split(' '))[ii]\n",
    "\n",
    "print(\"Bit   Bitval                 BitMeaning\")\n",
    "for bitval, meaning, bit in zip(bitvals, meanings, np.arange(len(bitvals))):\n",
    "    print(f\"{str(bit).zfill(2)}    {str(bitval).zfill(20)}   {meaning}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll use a quick function from the eumar tools library that allows us to read in the flag information simply by supplying the name (Bitmeaning as listed above). First we'll read in land, then a series of flags that all relate to cloud. \n",
    "\n",
    "If you want to look in detail at this function, you can add another code cell below and run the following command: print(inspect.getsource(eumartools.flag_mask.)) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reading masks: example land\n",
    "land_mask = eumartools.flag_mask(flag_file, 'WQSF', ['LAND'])\n",
    "land_mask = land_mask.astype(float)[::grid_reduce, ::grid_reduce]\n",
    "land_mask[land_mask == 0.0] = np.nan"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reading masks: example cloud\n",
    "cloud_mask = eumartools.flag_mask(flag_file, 'WQSF', ['CLOUD', 'CLOUD_AMBIGUOUS', 'CLOUD_MARGIN'])\n",
    "cloud_mask = cloud_mask.astype(float)[::grid_reduce, ::grid_reduce]\n",
    "cloud_mask[cloud_mask == 0.0] = np.nan"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll create a combine mash by reading in all the flags that are relevant for both the CHL_OC4ME and CHL_NN products. These lists are the recommended flags provided by EUMETSAT. You can find out more about them [here](https://eumetsatspace.atlassian.net/wiki/spaces/SEN3/pages/1688829976/OLCI+level-2+flags)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reading masks: all flags BAC recommended\n",
    "BAC_mask = eumartools.flag_mask(flag_file, 'WQSF', ['LAND', 'CLOUD', 'CLOUD_AMBIGUOUS', 'CLOUD_MARGIN', \n",
    "                                                    'INVALID', 'COSMETIC', 'SATURATED', 'SUSPECT',\n",
    "                                                    'HISOLZEN', 'HIGHGLINT', 'SNOW_ICE', 'AC_FAIL',\n",
    "                                                    'WHITECAPS', 'ADJAC', 'RWNEG_O2', 'RWNEG_O3',\n",
    "                                                    'RWNEG_O4', 'RWNEG_O5', 'RWNEG_O6', 'RWNEG_O7', 'RWNEG_O8',\n",
    "                                                    'OC4ME_FAIL'])\n",
    "BAC_mask = BAC_mask.astype(float)[::grid_reduce, ::grid_reduce]\n",
    "BAC_mask[BAC_mask == 0.0] = np.nan"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reading masks: all flags NN recommended\n",
    "NN_mask = eumartools.flag_mask(flag_file, 'WQSF', ['LAND', 'CLOUD', 'CLOUD_AMBIGUOUS', 'CLOUD_MARGIN', \n",
    "                                                    'INVALID', 'COSMETIC', 'SATURATED', 'SUSPECT',\n",
    "                                                    'HISOLZEN', 'HIGHGLINT', 'SNOW_ICE', 'AC_FAIL',\n",
    "                                                    'WHITECAPS', 'ADJAC', 'RWNEG_O2', 'RWNEG_O3',\n",
    "                                                    'RWNEG_O4', 'RWNEG_O5', 'RWNEG_O6', 'RWNEG_O7', 'RWNEG_O8',\n",
    "                                                    'OCNN_FAIL'])\n",
    "NN_mask = NN_mask.astype(float)[::grid_reduce, ::grid_reduce]\n",
    "NN_mask[NN_mask == 0.0] = np.nan"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## <a id='section4'></a>4. Plotting and comparing chlorophyll\n",
    "[Back to top](#TOC_TOP)\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we've read in both the data we're interested in, and the flags to creat a mask, we can plot the data on a map..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(1, 2, figsize=(10, 10), dpi=300, subplot_kw={\"projection\": ccrs.Mercator()})\n",
    "\n",
    "for m, var, cmap, all_mask in zip(axs.reshape(-1),\n",
    "                         ['CHL_OC4ME', 'CHL_NN'],\n",
    "                         ['viridis','viridis'],[BAC_mask, NN_mask]):\n",
    "    \n",
    "    p1 = m.pcolormesh(lon, lat, band_vars[var][::grid_reduce, ::grid_reduce], transform=ccrs.PlateCarree(), \n",
    "                       cmap=cmap, vmin=-2, vmax=2, zorder=1)\n",
    "\n",
    "    m.contourf(lon, lat, all_mask, levels=[0,1], transform=ccrs.PlateCarree(), colors='0.75', zorder=2)\n",
    "    m.contourf(lon, lat, cloud_mask, levels=[0,1], transform=ccrs.PlateCarree(), colors='0.25', zorder=3)\n",
    "    m.contourf(lon, lat, land_mask, levels=[0,1], transform=ccrs.PlateCarree(), colors='0.5', zorder=4)\n",
    "\n",
    "    m.annotate(f\"{var.replace('_',': ')}\", xy=(0.1, 0.75), xycoords='axes fraction',\n",
    "            transform=ccrs.PlateCarree(), zorder=30, color='w')\n",
    "\n",
    "    # Embellish with gridlines\n",
    "    g1 = m.gridlines(draw_labels = True, zorder=20, color='0.0', linestyle='--', linewidth=0.5)\n",
    "    g1.xlocator = mticker.FixedLocator(np.arange(-180, 180, 5))\n",
    "    g1.ylocator = mticker.FixedLocator(np.arange(-90, 90, 5))\n",
    "    g1.top_labels = False\n",
    "    g1.right_labels = False\n",
    "    g1.xlabel_style = {'color': 'black'}\n",
    "    g1.ylabel_style = {'color': 'black'}\n",
    "    m.set(facecolor = \"1.0\")\n",
    "    m.axis('off')\n",
    "\n",
    "plt.tight_layout()\n",
    "\n",
    "cbar = fig.colorbar(p1, ax=[axs[:]], location='bottom', pad=0.05)\n",
    "ticks = [-2,-1,0,1,2]\n",
    "cbar.set_ticks(ticks)\n",
    "cbar.set_ticklabels([10**tick for tick in ticks])\n",
    "cbar.set_label('Chlorophyll concentration [mg.m$^{-3}$]')\n",
    "plt.savefig('1_6_OLCI_CHL.png', bbox_inches='tight')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the figure you can see that whilst the general patterns are similar, there are some noticeable differences between the two algorithms, particularly in the coastal regions. You can also see some differences in the flags. The dark grey cloud mask is the same applied to both images, as is the land mask, but the combined other flags (light grey) differ, reflecting the different approaches taken by the two algorithms. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can look in more detail at the differences by creating a plot showing the difference between the two algorithms..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(10, 10), dpi=300)\n",
    "m = plt.subplot(projection=ccrs.Mercator())\n",
    "\n",
    "p1 = m.pcolormesh(lon, lat, band_vars[\"CHL_DIFF\"][::grid_reduce, ::grid_reduce], transform=ccrs.PlateCarree(), \n",
    "                   cmap=plt.cm.RdBu_r, vmin=-10, vmax=10, zorder=1)\n",
    "\n",
    "m.contourf(lon, lat, BAC_mask, levels=[0,1], transform=ccrs.PlateCarree(), colors='0.75', zorder=2)\n",
    "m.contourf(lon, lat, NN_mask, levels=[0,1], transform=ccrs.PlateCarree(), colors='0.75', zorder=2)\n",
    "m.contourf(lon, lat, cloud_mask, levels=[0,1], transform=ccrs.PlateCarree(), colors='0.25', zorder=3)\n",
    "m.contourf(lon, lat, land_mask, levels=[0,1], transform=ccrs.PlateCarree(), colors='0.5', zorder=4)\n",
    "\n",
    "m.annotate(\"(CHL_OC4ME - CHL_NN)\\nDark grey: clouds\\nGrey: land\\nLight grey: all other recommended flags\",\n",
    "           xy=(0.1, 0.75), xycoords='axes fraction', transform=ccrs.PlateCarree(), zorder=30, color='w')\n",
    "\n",
    "# Embellish with gridlines\n",
    "g1 = m.gridlines(draw_labels = True, zorder=20, color='0.0', linestyle='--',linewidth=0.5)\n",
    "g1.xlocator = mticker.FixedLocator(np.arange(-180, 180, 5))\n",
    "g1.ylocator = mticker.FixedLocator(np.arange(-90, 90, 5))\n",
    "g1.top_labels = False\n",
    "g1.right_labels = False\n",
    "g1.xlabel_style = {'color': 'black'}\n",
    "g1.ylabel_style = {'color': 'black'}\n",
    "m.set(facecolor = \"1.0\")\n",
    "m.axis('off')\n",
    "\n",
    "cbar = fig.colorbar(p1, location='bottom', pad=0.05)\n",
    "cbar.set_label('Chlorophyll Difference (OC4ME - NN) [mg.m$^{-3}$]')\n",
    "plt.tight_layout()\n",
    "plt.savefig('1_6_OLCI_CHL_diff.png', bbox_inches='tight')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we can confirm what was suspected from the two images above. Both algorithms provide similar estimates of chlorophyll-a concentration away from land, however near the coasts, particularly in areas where there is strong sediment presence from rivers or tidal resuspension, the CHL_OC4ME overestimates relative to the CHL_NN. This is likely because the CHL_OC4ME is misattributing signal associated with sediments and coloured dissolved organic matter to chlorophyll-a. In comparison, the CHL_NN, which uses more spectral information, and is trained using data from a wide range of waters, may well be more accurately attributing the signal to sediments in these waters, and less to chlorphyll-a. \n",
    "\n",
    "This image nicely summarises the main reasoning for choosing each product. In the open ocean, the CHL_OC4ME product is sufficient for estimating chlorophyll-a, whilst in the coastal oceans, the CHL_NN may be more appropriate."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>\n",
    "<a href=\"../Index.ipynb\"><< Index</a>\n",
    "<br>\n",
    "<a href=\"./1_5_OLCI_radiance_reflectance_spectra.ipynb\"><< OLCI radiance and reflectance spectra</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"1_7_OLCI_light_environment.ipynb\">Assessing the light environment >></a>\n",
    "<hr>\n",
    "<a href=\"https://gitlab.eumetsat.int/eumetlab/ocean/\">View on GitLab</a> | <a href=\"https://training.eumetsat.int/\">EUMETSAT Training</a> | <a href=mailto:ops@eumetsat.int>Contact helpdesk for support </a> | <a href=mailto:Copernicus.training@eumetsat.int>Contact our training team to collaborate on and reuse this material</a></span></p>"
   ]
  }
 ],
 "metadata": {
  "description": "This Jupyter Notebook discusses how to use level-2 OLCI chlorophyll products.",
  "image": "../img/thumbs/1_6_OLCI_CHL_comparison_thumb.png",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  },
  "tags": {
   "category": "sensor_principles",
   "domain": "ocean",
   "level": [
    "L2"
   ],
   "satellite": "Sentinel-3",
   "sensor": "OLCI",
   "variable": [
    "chlorophyll"
   ]
  },
  "title": "Comparing OLCI chlorophyl products"
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
