{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src='../frameworks/img/EU-Copernicus-EUM-WEKEO_banner_logo.png' align='right' width='75%'></img>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"../Index.ipynb\"><< Index</a>\n",
    "<br>\n",
    "<a href=\"./1_3_OLCI_coverage.ipynb\"><< Determining OLCI product coverage</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"./1_5_OLCI_radiance_reflectance_spectra.ipynb\">OLCI radiance and reflectance spectra >></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color=\"#138D75\">**Copernicus Marine Training Service**</font> <br>\n",
    "**Copyright:** 2022 EUMETSAT <br>\n",
    "**License:** MIT"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<html>\n",
    "  <div style=\"width:100%\">\n",
    "    <div style=\"float:left\"><a href=\"https://trainhub.eumetsat.int/ocean/sensors/1_4_OLCI_plot_spatial_RGB.ipynb\"><img src=\"https://img.shields.io/badge/Launch-TrainHub-Blue.svg\" alt=\"Open in TrainHub\"></a></div>\n",
    "    <div style=\"float:left\"><p>&emsp;</p></div>\n",
    "    <div style=\"float:left\"><a href=\"https://mybinder.org/v2/gl/eumetlab%2Focean%2Fsensors%2Flearn-olci/main?labpath=1_OLCI_introductory/1_4_OLCI_plot_spatial_RGB.ipynb\"><img src=\"https://mybinder.org/badge_logo.svg\" alt=\"Open in Binder\"></a></div>\n",
    "    <div style=\"float:left\"><p>&emsp;</p></div>\n",
    "    <div style=\"float:left\"><a href=\"https://colab.research.google.com/github/eumetlab/ocean/sensors/learn-olci/blob/main/1_OLCI_introductory/1_4_OLCI_plot_spatial_RGB.ipynb\"><img src=\"https://colab.research.google.com/assets/colab-badge.svg\" alt=\"Open In Colab\" /></a></div>\n",
    "    <div style=\"float:left\"><p>&emsp;</p></div>\n",
    "    <div style=\"float:left\"><a href=\"https://studiolab.sagemaker.aws/import/github/eumetlab/ocean/sensors/learn-olci/blob/main/1_OLCI_introductory/1_4_OLCI_plot_spatial_RGB.ipynb\"><img src=\"https://studiolab.sagemaker.aws/studiolab.svg\" alt=\"Open In Studio Lab\"/></a></div>\n",
    "    <div style=\"float:left\"><p>&emsp;</p></div>\n",
    "    <div style=\"float:left\"><a href=\"https://pccompute.westeurope.cloudapp.azure.com/compute/hub/user-redirect/git-pull?repo=https://github.com/eumetlab/ocean/sensors/learn-olci&urlpath=1_OLCI_introductory/1_4_OLCI_plot_spatial_RGB.ipynb&branch=main\"><img src=\"https://img.shields.io/badge/Open-Planetary%20Computer-black?style=flat&logo=microsoft\" alt=\"Open in Planetary Computer\"/></a></div>\n",
    "  </div>\n",
    "</html>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "<h3>Learn OLCI: Introductory</h3></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "    \n",
    "<b>PREREQUISITES </b>\n",
    "    \n",
    "The following modules are prerequisites for this notebook:\n",
    "- **[1_1_OLCI_data_access.ipynb](1_1_OLCI_data_access.ipynb)** (this will download the required OLCI products for this notebook)\n",
    "\n",
    "</div>\n",
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1.4 OLCI bands and imagery\n",
    "\n",
    "### Data used\n",
    "\n",
    "| Product Description  | Data Store collection ID|  WEkEO HDA ID | Product Navigator |\n",
    "|:--------------------:|:-----------------------:|:-------------:|:-----------------:|\n",
    "| Sentinel-3 OLCI level-1B Full resolution | EO:EUM:DAT:0409 | EO:EUM:DAT:SENTINEL-3:OL_1_EFR___ | [link](https://navigator.eumetsat.int/product/EO:EUM:DAT:SENTINEL-3:OL_1_EFR___NTC?query=OLCI&filter=satellite__Sentinel-3&filter=instrument__OLCI&filter=processingLevel__Level%201%20Data&s=advanced) |\n",
    "\n",
    "### Learning outcomes\n",
    "\n",
    "At the end of this notebook you will know;\n",
    "* How the OLCI instrument collects data at different wavelengths to characterise the colour of the light reflected by the ocean, land, and atmosphere.\n",
    "* How to extract the data associated with these wavebands from the OLCI level-1 product.\n",
    "* How to combine the data from these different wavebands to create a true colour/\"Red Green Blue (RGB)\" image.\n",
    "* How to select band combinations and optimise the scaling to highlight specific features in the images.\n",
    "\n",
    "\n",
    "### Outline\n",
    "\n",
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## <a id='TOC_TOP'></a>Contents\n",
    "\n",
    "</div>\n",
    "    \n",
    " 1. [OLCI bands](#section1)\n",
    " 1. [Common RGB band recipes](#section2)\n",
    " 1. [Improving RGBs](#section3)\n",
    " 1. [Mapping RGBs](#section4)\n",
    " 1. [Applying your knowledge](#section5)\n",
    "\n",
    "<hr>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# library imports\n",
    "import cartopy.crs as ccrs\n",
    "import eumartools\n",
    "import glob\n",
    "import inspect\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.ticker as mticker\n",
    "import numpy as np\n",
    "import os\n",
    "import warnings\n",
    "import xarray as xr\n",
    "\n",
    "warnings.filterwarnings('ignore')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## <a id='section1'></a>1. OLCI bands\n",
    "[Back to top](#TOC_TOP)\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Diagram of OLCI bands vs spectrum\n",
    "\n",
    "OLCI has 21 narrow spectral bands that detect at various wavelengths between 400 nm and 1020 nm. A subset of these wavelengths is shown in the image below. All bands provide data at the same 300m spatial resolution.\n",
    "\n",
    "<img src='../img/OLCI_spectral_1200.png' align='centre' width='75%'></img>\n",
    "<center>Figure 1: The spectral resolution of Sentinel-3 OLCI.</center>\n",
    "<br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at some data from the different wavebands in a level-1B OLCI product..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# selecting SAFE directory\n",
    "SAFE_directory = os.path.join(os.getcwd(), 'products', \n",
    "    'S3A_OL_1_EFR____20210717T101015_20210717T101315_20210718T145224_0179_074_122_1980_MAR_O_NT_002.SEN3')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the next block of code, we'll set some parameters that we'll then use when reading in the data from the different wavebands.\n",
    "\n",
    "First we'll reduce the scale of the data - we are plotting a full scene, so we don't really need the highest resolution available At this scale, the difference in resolution will be negligible once plotted, but make our plotting much faster. If you were looking at a smaller region/conducting a quantitative analysis, you would not use this approach. \n",
    "\n",
    "Then we'll use the glob command to find all the bands - we can identify all the band files because they have the \"Oa\" prefix. We don't need to open all the bands, as some are used for atmospheric correction and won't provide us with more information about the colour of the surfaces we are interested in, so we will skip opening these."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "grid_reduce = 3\n",
    "band_vars = xr.open_mfdataset(glob.glob(os.path.join(SAFE_directory,'Oa*.nc')))\n",
    "band_dict = {}\n",
    "bands_needed = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 17]\n",
    "\n",
    "for band_var in band_vars:\n",
    "    band_num = int(band_var.split('_')[0][2:])\n",
    "    if band_num not in bands_needed:\n",
    "        print(f\"Skipping: {band_var}, as we don't need it for now\")\n",
    "    else:\n",
    "        print(f\"Reading:  {band_var}\")\n",
    "        band_dict[band_var] = band_vars[band_var].data[::grid_reduce,::grid_reduce]\n",
    "\n",
    "band_vars.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## <a id='section1'></a>2. Common RGB band recipes\n",
    "[Back to top](#TOC_TOP)\n",
    "\n",
    "</div>\n",
    "\n",
    "Our eyes contain three types of cones (short, medium and long-wave) that detect light in relatively broad bands centred on 437 nm, 533 nm and 564 nm. These wavelengths approximately map to the wavelengths of blue (\\~450 nm), green (\\~550 nm) and red light (\\~650 nm).\n",
    "\n",
    "We create an RGB image by mapping any of these wavelengths to the \"red\", \"green\" and \"blue\" channels that our eyes detect. It is important to remember, though, that \"RGB\" refers to the channels in the image we produce, and <font color=\"#FF0000\">**not necessarily**</font> the bands we use to create the channels.\n",
    "\n",
    "When we map the appropriate bands for the wavelengths of red (\\~650 nm), green (\\~550 nm) and blue (\\~450 nm) light to the RGB channels, then we call this a *true colour* or *natural colour* image. When we use other band combinations, we call this a *false colour* image. Sometimes, to highlight specific features, we drift between the two or overlay. However, our eyes do not detect intensity linearly, so there are a few tricks we can use to improve our images.\n",
    "\n",
    "Band recipes for RGB images are very varied, but here are some common ones\n",
    "\n",
    "* <font color=\"#138D75\">**natural colour single band**</font> - this uses the nearest bands to red, green and blue light; for OLCI these are bands 8, 6 & 2)\n",
    "* <font color=\"#138D75\">**tristimulus**</font> - this applies the spectral response function of our eyes to OLCI, utilising 10 bands\n",
    "* <font color=\"#138D75\">**natural colour broad band, log scaled**</font> - this takes a broader selection of bands for each channel and log scales the intensity to our visual system\n",
    "* <font color=\"#138D75\">**false colour**</font> - this maps any selection of bands you like to RGB, and is useful for feature extraction, e.g. 17, 6, 2 for near-infrared sensitivity in the \"red\" channel\n",
    "\n",
    "Below, we will build these four band recipes and compare how they look for our test scene. Note that, in each case, we normalise our image array using the `eumartools.normalise_image` method. This will re-scale the image range to values between 0 and 1, which our plotting routine requires. For more information on the method, you can run ```print(inspect.getsource(eumartools.normalise_image))```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Natural colour single band\n",
    "red_recipe = band_dict['Oa08_radiance']\n",
    "green_recipe = band_dict['Oa06_radiance']\n",
    "blue_recipe = band_dict['Oa02_radiance']\n",
    "\n",
    "rgb_nat = np.dstack((red_recipe, green_recipe, blue_recipe))\n",
    "rgb_nat_norm = eumartools.normalise_image(rgb_nat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Tristimulus\n",
    "red_recipe = np.log10(1.0 + 0.01 * band_dict['Oa01_radiance'] + 0.09 * band_dict['Oa02_radiance'] \n",
    "                      + 0.35 * band_dict['Oa03_radiance'] + 0.04 * band_dict['Oa04_radiance'] \n",
    "                      + 0.01 * band_dict['Oa05_radiance'] + 0.59 * band_dict['Oa06_radiance'] \n",
    "                      + 0.85 * band_dict['Oa07_radiance'] + 0.12 * band_dict['Oa08_radiance'] \n",
    "                      + 0.07 * band_dict['Oa09_radiance'] + 0.04 * band_dict['Oa10_radiance'])\n",
    "green_recipe = np.log10(1.0 + 0.26 * band_dict['Oa03_radiance'] + 0.21 * band_dict['Oa04_radiance'] \n",
    "                        + 0.50 * band_dict['Oa05_radiance'] + band_dict['Oa06_radiance'] \n",
    "                        + 0.38 * band_dict['Oa07_radiance'] + 0.04 * band_dict['Oa08_radiance'] \n",
    "                        + 0.03 * band_dict['Oa09_radiance'] + 0.02 * band_dict['Oa10_radiance'])\n",
    "blue_recipe = np.log10(1.0 + 0.07 * band_dict['Oa01_radiance'] + 0.28 * band_dict['Oa02_radiance'] \n",
    "                       + 1.77 * band_dict['Oa03_radiance'] + 0.47 * band_dict['Oa04_radiance'] \n",
    "                       + 0.16 * band_dict['Oa05_radiance'])\n",
    "\n",
    "rgb_tri = np.dstack((red_recipe, green_recipe, blue_recipe))\n",
    "rgb_tri_norm = eumartools.normalise_image(rgb_tri)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Natural colour broad band, log scaled\n",
    "red_recipe = 0.16666 * band_dict['Oa09_radiance'] + 0.66666 * band_dict['Oa09_radiance'] \\\n",
    "             + 0.08333 * band_dict['Oa10_radiance'] + 0.08333 * band_dict['Oa11_radiance']\n",
    "green_recipe = 0.16666 *  band_dict['Oa05_radiance'] + 0.66666 *  band_dict['Oa06_radiance'] \\\n",
    "               + 0.16666 *  band_dict['Oa07_radiance']\n",
    "blue_recipe = 0.16666 *  band_dict['Oa02_radiance'] + 0.66666 *  band_dict['Oa03_radiance'] \\\n",
    "               + 0.16666 *  band_dict['Oa04_radiance']\n",
    "\n",
    "rgb_log = np.dstack(((np.log10(red_recipe * 0.01) + 1.6516951369518393) / 1.9887713527138795,\n",
    "                    (np.log10(green_recipe * 0.01) + 1.6516951369518393) / 1.9887713527138795,\n",
    "                    (np.log10(blue_recipe * 0.01) + 1.6516951369518393) / 1.9887713527138795))\n",
    "                     \n",
    "rgb_log_norm = eumartools.normalise_image(rgb_log)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# False colour\n",
    "red_recipe = band_dict['Oa17_radiance']\n",
    "green_recipe = band_dict['Oa06_radiance']\n",
    "blue_recipe = band_dict['Oa02_radiance']\n",
    "\n",
    "rgb_fls = np.dstack((red_recipe, green_recipe, blue_recipe))\n",
    "rgb_fls_norm = eumartools.normalise_image(rgb_fls)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(2, 2, figsize=(10, 8), dpi=150)\n",
    "\n",
    "axs[0,0].imshow(rgb_nat_norm); axs[0,0].title.set_text('Natural colour single channel, 8:6:2')\n",
    "axs[0,1].imshow(rgb_tri_norm); axs[0,1].title.set_text('Tristimulus')\n",
    "axs[1,0].imshow(rgb_log_norm); axs[1,0].title.set_text('Natural colour broad channel, logged')\n",
    "axs[1,1].imshow(rgb_fls_norm); axs[1,1].title.set_text('False colour 17:6:2')\n",
    "\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Above we can see that there are a lot of differences in images created from OLCI data depending on which band combinations you choose. You can see that there's some advantages and disadvantages to the different choices. Both Tristimulus and Natural colour broad band give an approximate visualisation of what you might expect the Earth to look like if you were in space looking down with your own eyes. The False colour 17:6:2 image very distinctly shows the differences between water, land, and clouds, but isn't particularly helpful for identifying different features on land or in the oceans. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## <a id='section3'></a>3. Improving RGBs\n",
    "[Back to top](#TOC_TOP)\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a number of ways that we can improve our RGB images. For instance we can:\n",
    "* `normalise` our image by channel (\"unhitch\"-ing them), as opposed to above where we normalised by all channels.\n",
    "* `histogram` our image, rebinning it in \"n\" bins to reduce the dynamic range, normalisation required first!\n",
    "* `truncate` our image, cutting out some of the outliers based on percentiles\n",
    "\n",
    "Lets try some of these examples below, using `rgb_log` (or `rgb_log_norm`) or as the basis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rgb_log_norm_unhitch = eumartools.normalise_image(rgb_log, unhitch=True)\n",
    "rgb_log_norm_trunc = eumartools.truncate_image(rgb_log_norm, min_percentile=5.0, max_percentile=95.0)\n",
    "rgb_log_norm_hist = eumartools.histogram_image(rgb_log_norm, nbins=512)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots(2, 2, figsize=(10, 8), dpi=150)\n",
    "\n",
    "axs[0,0].imshow(rgb_log_norm); axs[0,0].title.set_text('Natural colour broad channel, logged, normaliased')\n",
    "axs[0,1].imshow(rgb_log_norm_unhitch); axs[0,1].title.set_text('Normalised by channel (unhitched)')\n",
    "axs[1,0].imshow(rgb_log_norm_trunc); axs[1,0].title.set_text('Truncated (normalised)')\n",
    "axs[1,1].imshow(rgb_log_norm_hist); axs[1,1].title.set_text('Histogrammed (normalised)')\n",
    "\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As in the previous examples, we can see differences between the approaches used above. The different choices made allow us to highlight different features, and in the final one we can see that we are able to identify a range of different features in the ocean."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## <a id='section4'></a>4. Mapping RGBs\n",
    "[Back to top](#TOC_TOP)\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far we have only shown images displayed against pixel coordinates (rows & columns). If we want to display this on geographical coordinates we need to use our longitude and latitude variables. The OLCI level-1b and level-2 grids are \"curvilinear\", so our longitude and latitude coordinates are both 2-dimensional arrays. The most convenient way to plot this in python is to use the matplotlib `pcolormesh` routine, and to map each pixel to an RGB colour."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "geo_file = os.path.join(SAFE_directory,'geo_coordinates.nc')\n",
    "geo_vars = xr.open_dataset(geo_file)\n",
    "lon = geo_vars.longitude.data[::grid_reduce,::grid_reduce]\n",
    "lat = geo_vars.latitude.data[::grid_reduce,::grid_reduce]\n",
    "geo_vars.close()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(10, 8), dpi=150)\n",
    "m = plt.subplot(projection=ccrs.Mercator())\n",
    "\n",
    "# make the colour map\n",
    "colorArray = np.array(rgb_log_norm_unhitch)\n",
    "colorTuple = colorArray.reshape((colorArray.shape[0] * colorArray.shape[1]), 3)\n",
    "colorTuple = np.insert(colorTuple, 3, 1.0, axis=1)\n",
    "\n",
    "# make the map\n",
    "m.pcolormesh(lon, lat, lon, color=colorTuple, edgecolors=None, transform=ccrs.PlateCarree())\n",
    "\n",
    "# Embellish with gridlines\n",
    "g1 = m.gridlines(draw_labels = True, zorder=20, color='k', linestyle='--',linewidth=0.5)\n",
    "g1.xlocator = mticker.FixedLocator(np.arange(-180, 180, 2))\n",
    "g1.ylocator = mticker.FixedLocator(np.arange(-90, 90, 2))\n",
    "g1.top_labels = False\n",
    "g1.right_labels = False\n",
    "g1.xlabel_style = {'size': 10, 'color': 'black'}\n",
    "g1.ylabel_style = {'size': 10, 'color': 'black'}\n",
    "m.set(facecolor = \"1.0\")\n",
    "m.axis('off')\n",
    "\n",
    "plt.title('Geographically mapped OLCI Level-1b scene', fontsize=10)\n",
    "plt.savefig('1_4_OLCI_RGB.png', bbox_inches='tight')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-danger\" role=\"alert\">\n",
    "\n",
    "## <a id='section5'></a>5. Applying your knowledge\n",
    "[Back to top](#TOC_TOP)\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "\n",
    "### Challenge:\n",
    "\n",
    "Try using the routines provided to create a geographically mapped scene, that highlights the phytoplankton signals.\n",
    " <div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden",
    "solution2_first": true
   },
   "outputs": [],
   "source": [
    "# Enter your solution here\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "solution2": "hidden"
   },
   "outputs": [],
   "source": [
    "# first normalise by channel\n",
    "rgb_log_norm_unhitch = eumartools.normalise_image(rgb_log, unhitch=True)\n",
    "# then remove the outliers from the unhitched image\n",
    "rgb_log_norm_unhitch_trunc = eumartools.truncate_image(rgb_log_norm_unhitch)\n",
    "# then histogram the truncate image\n",
    "rgb_log_final = eumartools.histogram_image(rgb_log_norm_unhitch_trunc, nbins=512)\n",
    "\n",
    "# make the plot\n",
    "fig = plt.figure(figsize=(10, 8), dpi=300)\n",
    "m = plt.subplot(projection=ccrs.Mercator())\n",
    "\n",
    "# make the colour map\n",
    "colorArray = np.array(rgb_log_final)\n",
    "colorTuple = colorArray.reshape((colorArray.shape[0] * colorArray.shape[1]), 3)\n",
    "colorTuple = np.insert(colorTuple, 3, 1.0, axis=1)\n",
    "\n",
    "# make the map\n",
    "m.pcolormesh(lon, lat, lon, color=colorTuple, edgecolors=None, transform=ccrs.PlateCarree())\n",
    "\n",
    "# Embellish with gridlines\n",
    "g1 = m.gridlines(draw_labels = True, zorder=20, color='k', linestyle='--',linewidth=0.5)\n",
    "g1.xlocator = mticker.FixedLocator(np.arange(-180, 180, 2))\n",
    "g1.ylocator = mticker.FixedLocator(np.arange(-90, 90, 2))\n",
    "g1.xlabels_top = False\n",
    "g1.ylabels_right = False\n",
    "g1.xlabel_style = {'size': 10, 'color': 'black'}\n",
    "g1.ylabel_style = {'size': 10, 'color': 'black'}\n",
    "m.set(facecolor = \"1.0\")\n",
    "m.axis('off')\n",
    "\n",
    "plt.title('Geographically mapped OLCI Level-1b scene', fontsize=10)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What to try next?\n",
    "\n",
    "* Try choose your own OLCI Level-1 product and create your own RGB image.\n",
    "* Try adapting this script to read an OLCI Level-2 and create your own RGB image <font color=\"#138D75\">**(hint: \"reflectance\" replaces \"radiance\" at level-2)**</font>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>\n",
    "<a href=\"../Index.ipynb\"><< Index</a>\n",
    "<br>\n",
    "<a href=\"./1_3_OLCI_coverage.ipynb\"><< Determining OLCI product coverage</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"./1_5_OLCI_radiance_reflectance_spectra.ipynb\">OLCI radiance and reflectance spectra >></a>\n",
    "<hr>\n",
    "<a href=\"https://gitlab.eumetsat.int/eumetlab/ocean/\">View on GitLab</a> | <a href=\"https://training.eumetsat.int/\">EUMETSAT Training</a> | <a href=mailto:ops@eumetsat.int>Contact helpdesk for support </a> | <a href=mailto:Copernicus.training@eumetsat.int>Contact our training team to collaborate on and reuse this material</a></span></p>"
   ]
  }
 ],
 "metadata": {
  "description": "This Jupyter Notebook discuss the radiance and reflectance bands in OLCI level-1b and level-2 products.",
  "image": "../img/thumbs/1_4_OLCI_bands_imagery_thumb.png",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  },
  "tags": {
   "category": "data_discovery",
   "domain": "ocean",
   "level": [
    "L1",
    "L2"
   ],
   "satellite": "Sentinel-3",
   "sensor": "OLCI",
   "variable": [
    "radiance",
    "reflectance"
   ]
  },
  "title": "OLCI bands and imagery"
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
