{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src='../frameworks/img/EU-Copernicus-EUM-WEKEO_banner_logo.png' align='right' width='75%'></img>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"../Index.ipynb\"><< Index</a>\n",
    "<br>\n",
    "<a href=\"./1_2_OLCI_file_structure.ipynb\">Understanding OLCI product structure >></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<font color=\"#138D75\">**Copernicus Marine Training Service**</font> <br>\n",
    "**Copyright:** 2022 EUMETSAT <br>\n",
    "**License:** MIT"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<html>\n",
    "  <div style=\"width:100%\">\n",
    "    <div style=\"float:left\"><a href=\"https://trainhub.eumetsat.int/ocean/sensors/1_1_OLCI_data_access.ipynb\"><img src=\"https://img.shields.io/badge/Launch-TrainHub-Blue.svg\" alt=\"Open in TrainHub\"></a></div>\n",
    "    <div style=\"float:left\"><p>&emsp;</p></div>\n",
    "    <div style=\"float:left\"><a href=\"https://mybinder.org/v2/gl/eumetlab%2Focean%2Fsensors%2Flearn-olci/main?labpath=1_OLCI_introductory/1_1_OLCI_data_access.ipynb\"><img src=\"https://mybinder.org/badge_logo.svg\" alt=\"Open in Binder\"></a></div>\n",
    "    <div style=\"float:left\"><p>&emsp;</p></div>\n",
    "    <div style=\"float:left\"><a href=\"https://colab.research.google.com/github/eumetlab/ocean/sensors/learn-olci/blob/main/1_OLCI_introductory/1_1_OLCI_data_access.ipynb\"><img src=\"https://colab.research.google.com/assets/colab-badge.svg\" alt=\"Open In Colab\" /></a></div>\n",
    "    <div style=\"float:left\"><p>&emsp;</p></div>\n",
    "    <div style=\"float:left\"><a href=\"https://studiolab.sagemaker.aws/import/github/eumetlab/ocean/sensors/learn-olci/blob/main/1_OLCI_introductory/1_1_OLCI_data_access.ipynb\"><img src=\"https://studiolab.sagemaker.aws/studiolab.svg\" alt=\"Open In Studio Lab\"/></a></div>\n",
    "    <div style=\"float:left\"><p>&emsp;</p></div>\n",
    "    <div style=\"float:left\"><a href=\"https://pccompute.westeurope.cloudapp.azure.com/compute/hub/user-redirect/git-pull?repo=https://github.com/eumetlab/ocean/sensors/learn-olci&urlpath=1_OLCI_introductory/1_1_OLCI_data_access.ipynb&branch=main\"><img src=\"https://img.shields.io/badge/Open-Planetary%20Computer-black?style=flat&logo=microsoft\" alt=\"Open in Planetary Computer\"/></a></div>\n",
    "  </div>\n",
    "</html>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "<h3>Learn OLCI: Introductory</h3></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "    \n",
    "<b>PREREQUISITES </b>\n",
    "    \n",
    "This notebook has the following prerequisites:\n",
    "- **[A WEkEO account](https://my.wekeo.eu/user-registration)** if you are using or plan to use WEkEO\n",
    "- **[A EUMETSAT Earth Observation Portal account](https://eoportal.eumetsat.int/)** if you are using or plan to use the EUMETSAT Data Store or CODA\n",
    "  \n",
    "\n",
    "There are no prerequisite notebooks for this module.\n",
    "</div>\n",
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1.1 Accessing OLCI data \n",
    "\n",
    "### Data used\n",
    "\n",
    "| Product Description  | Data Store collection ID|  WEkEO HDA ID | Product Navigator |\n",
    "|:--------------------:|:-----------------------:|:-------------:|:-----------------:|\n",
    "| Sentinel-3 OLCI level-1B Full resolution | EO:EUM:DAT:0409 | EO:EUM:DAT:SENTINEL-3:OL_1_EFR___ | [link](https://navigator.eumetsat.int/product/EO:EUM:DAT:SENTINEL-3:OL_1_EFR___NTC?query=OLCI&filter=satellite__Sentinel-3&filter=instrument__OLCI&filter=processingLevel__Level%201%20Data&s=advanced) |\n",
    "| Sentinel-3 OLCI level-2 full resolution  | EO:EUM:DAT:0407 | EO:EUM:DAT:SENTINEL-3:OL_2_WFR___ | [link](https://navigator.eumetsat.int/product/EO:EUM:DAT:SENTINEL-3:OL_2_WFR___NTC?query=OLCI&filter=satellite__Sentinel-3&filter=instrument__OLCI&filter=processingLevel__Level%202%20Data&s=advanced) |\n",
    "| Sentinel-3 OLCI level-2 reduced resolution | EO:EUM:DAT:0408 | WEkEO: EO:EUM:DAT:SENTINEL-3:OL_2_WRR___ | [link](https://navigator.eumetsat.int/product/EO:EUM:DAT:SENTINEL-3:OL_2_WRR___NTC?query=OLCI&filter=satellite__Sentinel-3&filter=instrument__OLCI&filter=processingLevel__Level%202%20Data&s=advanced) |\n",
    "\n",
    "### Learning outcomes\n",
    "\n",
    "At the end of this notebook you will know;\n",
    "* which <font color=\"#138D75\">**web portals**</font> you can use to access OLCI data\n",
    "* how to download data from the EUMETSAT Data Store using the <font color=\"#138D75\">**eumetsat data access (eumdac)**</font> client\n",
    "* how to download data from WEkEO using the <font color=\"#138D75\">**harmonised data access (hda)**</font> client\n",
    "\n",
    "### Outline\n",
    "\n",
    "Data from OLCI is available through multiple sources, either via a web user interface (WebUI) or through code and command line interfaces with an Application Programming Interface (API). WebUIs are useful for accessing quick-look data visualisations, and for browsing to see what is available. APIs are more useful for routine, automated and operational data access. Here we will guide you through ways you can access OLCI data through both methods.\n",
    "\n",
    "Users should note that this notebook will use APIs to download <font color=\"#138D75\">**all**</font> of the products required for the lessons in the OLCI introductory module. However, there is <font color=\"#FF0000\">**no need to run both**</font> the [Downloading via the Data Store API](#Data_Store_API) and [Downloading via the WEkEO API](#WEKEO_API) sections as they achieve the same goal, via different means. Which option you choose, depends on where you are working (locally, or on a hosted JupyterHub such a that on WEkEO), and (more generally speaking), whether you are wanting to make multiple calls to the same API. If you were working with OLCI and other data served by EUMETSAT, the Data Store may be your preferred choice. If you are working with OLCI and other Copernicus data (such as that from the Copernicus Marine Service), the WEkEO API may serve you better. Users working within the WEkEO environment (JupyterHub or virtual machine) should opt for the [Downloading via the WEkEO API](#WEKEO_API) approach, as this will be faster.\n",
    "\n",
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## <a id='TOC_TOP'></a>Contents\n",
    "\n",
    "</div>\n",
    "    \n",
    " 1. [Download data via GUIs](#section1)\n",
    " 1. [The products we need](#section2)\n",
    " 1. [Downloading via the Data Store API](#section3)\n",
    " 1. [Downloading via the WEkEO API](#section4)\n",
    "\n",
    "<hr>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import configparser\n",
    "import shutil\n",
    "import os\n",
    "import json\n",
    "import zipfile\n",
    "from IPython.display import YouTubeVideo, HTML\n",
    "\n",
    "import eumdac #for downloading via the eumetsat/data-store\n",
    "from hda import Client #for downloading via wekeo\n",
    "\n",
    "# set defaults and overwrite with frameworks config if it exists\n",
    "config = configparser.ConfigParser()\n",
    "config[\"nbook\"] = {\"v_wd\" : \"700\", \"v_ht\" : \"450\"}\n",
    "if os.path.exists(os.path.join(os.path.dirname(os.getcwd()),\"frameworks\",\"config.ini\")):\n",
    "    config.read(os.path.join(os.path.dirname(os.getcwd()),\"frameworks\",\"config.ini\"))\n",
    "\n",
    "# Create a download directory for our OLCI products\n",
    "download_dir = os.path.join(os.getcwd(), \"products\")\n",
    "os.makedirs(download_dir, exist_ok=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## <a id='section1'></a>1. Downloading data via GUIs\n",
    "[Back to top](#TOC_TOP)\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The EUMETSAT Data Store\n",
    "\n",
    "The [Data Store](https://data.eumetsat.int) is EUMETSAT's primary pull service for delivering data, including the ocean data available from Sentinel-3 and OLCI. \n",
    "\n",
    "Access to it is possible through a WebUI, and through a series of application programming interfaces (APIs). The Data Store supports browsing, searching and downloading data as well as subscription services. It also provides a link to the online version of the [EUMETSAT Data Tailor](https://tailor.eumetsat.int/) for customisation. The video below provides an overview of the Data Store WebUI.\n",
    "\n",
    "The video below explains how you can access data through the EUMETSAT Data Store WebUI. You can visit and see if you can find OLCI data to download, however this notebook will also show you how to access the Data Store API, facilitated by the EUMDAC client (see the [Downloading via the Data Store API](#section3) section, below).\n",
    "#### Links:\n",
    "\n",
    "* [EUMETSAT Data Store](https://data.eumetsat.int)\n",
    "* [More information on the Data Store](https://eumetsatspace.atlassian.net/wiki/spaces/DSDS/overview)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "YouTubeVideo('WINakzAZvxw', width=config[\"nbook\"][\"v_wd\"], height=config[\"nbook\"][\"v_ht\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>\n",
    "\n",
    "### WEkEO\n",
    "\n",
    "WEkEO is the Copernicus Data Information and Access Service (DIAS) reference service for environmental data. It is a partnership between EUMETSAT, the European Centre for Medium-range Weather Forecasting (ECMWF), Mercator Ocean International, and the European Environment Agency (EEA). You can see an overview of the WEkEO service offering in the video below. WEkEO offers a harmonised data access (HDA) solution to a wide range of Copernicus data, including that from Sentinel-3 and associated down-stream products from the Copernicus Marine Service. The WEkEO HDA can be accessed via the [WEkEO Data Viewer](https://www.wekeo.eu/data) and through an API, see [Downloading from WEkEO via the API](#section4).\n",
    "\n",
    "You can visit the [WEkEO Data Viewer](https://www.wekeo.eu/data) and explore the available marine data, however this notebook will also show you how to use the HDA API - see [Downloading from WEkEO via the API](#section4) below.\n",
    "\n",
    "#### Links:\n",
    "* [WEkEO Data Viewer](https://www.wekeo.eu/data)\n",
    "* [More information on WEkEO](https://www.wekeo.eu/docs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "YouTubeVideo('UOexmq1LdyY', width=config[\"nbook\"][\"v_wd\"], height=config[\"nbook\"][\"v_ht\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>\n",
    "\n",
    "### The Copernicus Online Data Access (CODA) Portal\n",
    "\n",
    "The [Copernicus Online Data Access (CODA)](https://coda.eumetsat.int) portal will continue to serve Sentinel-3 data while data is transferred to the new EUMETSAT Data Store. It can be accessed via a WebUI and programmatically using an API (see example code on the [EUMETSAT GitLab](https://gitlab.eumetsat.int/eumetlab/cross-cutting-tools/sentinel-downloader)). \n",
    "\n",
    "In this notebook we won't use CODA but if you are searching for data on the EUMETSAT Data Store or WEkEO and find it is unavailable, it may be available via CODA, [CODAREP (for reprocessed data)](https://codarep.eumetsat.int), or the EUMETSAT [Data Centre Archive](https://archive.eumetsat.int). \n",
    "\n",
    "The video below shows how to use the CODA WebUI to search for, and download, OLCI data.\n",
    "\n",
    "#### Links:\n",
    "\n",
    "* [CODA](https://coda.eumetsat.int)\n",
    "* [More information on CODA](https://coda.eumetsat.int/manual/CODA-user-manual.pdf)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "YouTubeVideo('V3NAuafvlFM', width=config[\"nbook\"][\"v_wd\"], height=config[\"nbook\"][\"v_ht\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## 2. <a id='section2'></a>The products we need for this module\n",
    "[Back to top](#TOC_TOP)\n",
    "    \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need a number of OLCI level-1b and level-2 products for the notebooks within the introductory part of the learn-olci module. These products are shown below in a python `list` called *product list*. We can retrieve these products from either the Data Store or the WEkEO product catalogue."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "product_list = [\n",
    "'S3A_OL_1_EFR____20210717T101015_20210717T101315_20210718T145224_0179_074_122_1980_MAR_O_NT_002.SEN3',\n",
    "'S3A_OL_2_WRR____20210717T095732_20210717T104152_20210718T152419_2660_074_122______MAR_O_NT_003.SEN3',\n",
    "'S3A_OL_2_WFR____20210717T101015_20210717T101315_20210718T221347_0179_074_122_1980_MAR_O_NT_003.SEN3'\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>\n",
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## 3. <a id='section3'></a>Downloading from the Data Store via API\n",
    "[Back to top](#TOC_TOP)\n",
    "\n",
    "</div>\n",
    "\n",
    "The first option you can use to access OLCI data for these notebooks, is the [EUMETSAT Data Store](https://data.eumetsat.int). We will use a data access client called \"EUMDAC\" - the EUMETSAT Data Access Client to access the data. You should have installed the client following the environment yml file associated to the learn-olci repository, but you can also find the source code on the [EUMETSAT gitlab](https://gitlab.eumetsat.int/eumetlab/data-services/eumdac).Visit the EUMETSAT user support confluence spaces for the the [Data Store](https://eumetsatspace.atlassian.net/wiki/spaces/DSDS/overview) and [EUMDAC](https://eumetsatspace.atlassian.net/wiki/spaces/EUMDAC/overview).\n",
    "\n",
    "If you are working in WEkEO, please skip to [section 4](section_4).\n",
    "\n",
    "In order to allow us to download data from the Data Store via API, we need to provide our credentials. To do this, we need to create a file called `.eumdac_credentials` in our home directory. For most computer systems the home directory can be found at the path \\user\\username, /users/username, or /home/username depending on your operating system.\n",
    "\n",
    "In this file we need to add the following information exactly as follows;\n",
    "\n",
    "```\n",
    "{\n",
    "\"consumer_key\": \"<your_consumer_key>\",\n",
    "\"consumer_secret\": \"<your_consumer_secret>\"\n",
    "}\n",
    "```\n",
    "\n",
    "You must replace `<your_consumer_key>` and `<your_consumer_secret>` with the information you extract from https://api.eumetsat.int/api-key/. You will need a [EUMETSAT Earth Observation Portal account](https://eoportal.eumetsat.int/) to access this link, and in order to see the information you must click the \"Show hidden fields\" button at the bottom of the page.\n",
    "\n",
    "*Note: your key and secret are permanent, so you only need to do this once, but you should take care to never share them*\n",
    "\n",
    "Once you have done this, you can read in your credentials using the commands in the following cell. These will be used to generate a time-limited token, which will refresh itself when it expires."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(os.path.join(os.path.expanduser(\"~\"),'.eumdac_credentials')) as json_file:\n",
    "    credentials = json.load(json_file)\n",
    "    token = eumdac.AccessToken((credentials['consumer_key'], credentials['consumer_secret']))\n",
    "    print(f\"This token '{token}' expires {token.expiration}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have a token, we can see what OLCI specific collections we have in the Data Store."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "datastore = eumdac.DataStore(token)\n",
    "for collection_id in datastore.collections:\n",
    "    if \"OLCI\" in collection_id.title:\n",
    "        print(f\"Collection ID({collection_id}): {collection_id.title}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, for;\n",
    "* **OLCI Level 1B Full Resolution** we want `collection_id`: **EO:EUM:DAT:0409**.\n",
    "* **OLCI Level 2 Ocean Colour Reduced Resolution** we want `collection_id`: **EO:EUM:DAT:0408**\n",
    "* **OLCI Level 2 Ocean Colour Full Resolution** we want `collection_id`: **EO:EUM:DAT:0407**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets get some level-1B data first. Below we provide the level-1B `collection_id` to our `datastore` object to choose the correct collection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "collection_id = 'EO:EUM:DAT:0409'\n",
    "selected_collection = datastore.get_collection(collection_id)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, as we already know what product we want, we can now find the product directly using the **product name**. which in this case the first item in our product list. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "selected_product = datastore.get_product(product_id=product_list[0], collection_id=collection_id)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For more advanced search options using EUMDAC see the notebook on [OLCI advanced data access](../2_OLCI_advanced/2_1_OLCI_advanced_data_access_eumdac.ipynb).\n",
    "\n",
    "Now we can download the product. It will go into our download directory *(../products)*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with selected_product.open() as fsrc, open(os.path.join(download_dir, fsrc.name), mode='wb') as fdst:\n",
    "    print(f'Downloading {fsrc.name}')\n",
    "    shutil.copyfileobj(fsrc, fdst)\n",
    "    print(f'Download of product {fsrc.name} finished.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The product is downloaded as a zip file, so lets unzip it and remove the zip."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with zipfile.ZipFile(fdst.name, 'r') as zip_ref:\n",
    "    for file in zip_ref.namelist():\n",
    "        if file.startswith(str(selected_product)):\n",
    "            zip_ref.extract(file, download_dir)\n",
    "    print(f'Unzipping of product {selected_product} finished.')\n",
    "os.remove(fdst.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can now check the **products** directory to see that we have the level 1B file. Now, lets pull all the parts together and download both level 2 files in a single loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "collection_ids = [\"EO:EUM:DAT:0408\", \"EO:EUM:DAT:0407\"]\n",
    "\n",
    "for product_id, collection_id in zip(product_list[1:],collection_ids):\n",
    "    print(f\"Retrieving: {product_id}\")    \n",
    "\n",
    "    selected_collection = datastore.get_collection(collection_id)\n",
    "    selected_product = datastore.get_product(product_id=product_id, collection_id=collection_id)\n",
    "    \n",
    "    with selected_product.open() as fsrc, open(os.path.join(download_dir, fsrc.name), mode='wb') as fdst:\n",
    "        print(f'Downloading {fsrc.name}.')\n",
    "        shutil.copyfileobj(fsrc, fdst)\n",
    "        print(f'Download of product {fsrc.name} finished.')\n",
    "\n",
    "    with zipfile.ZipFile(fdst.name, 'r') as zip_ref:\n",
    "        for file in zip_ref.namelist():\n",
    "            if file.startswith(str(selected_product)):\n",
    "                zip_ref.extract(file, download_dir)\n",
    "        print(f'Unzipping of product {fdst.name} finished.')\n",
    "\n",
    "    os.remove(fdst.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you have completed this section, you can now continue to work through the rest of the notebooks in this repository and learn more about working with OLCI data. Alternatively, if you'd like to learn more about using the advanced functionality of EUMDAC to access OLCI data you can check out the advanced workflows in the <a href=\"../2_OLCI_advanced/2_1_OLCI_advanced_data_access_eumdac.ipynb\">Advanced data access with **eumdac**</a> notebook. \n",
    "\n",
    "If you're interested in, or currently working on, WEkEO, please continue to [section 4](section_4).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>\n",
    "<div class=\"alert alert-info\" role=\"alert\">\n",
    "\n",
    "## 4. <a id='section4'></a>Downloading from WEkEO via the API\n",
    "[Back to top](#TOC_TOP)\n",
    "\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Downloading from WEkEO harmonised data access (HDA) API is facilitated by another data access client. \n",
    "If you are currently working on the WEkEO JupyterHub, this client will already be installed. Otherwise, as with EUMDAC, if you have followed the installation guidelines in the yml file provided with this repository, you will also have the client installed. If not, you can find the source code [here](https://github.com/ecmwf/hda).\n",
    "\n",
    "In order to allow us to download data using the WEkEO HDA API, we need to provide our credentials. To do this, we need to create a file called `.hdarc` in our home directory. For most computer systems the home directory can be found at the path \\user\\username, /users/username, or /home/username depending on your operating system. In this file we need to add the following information exactly as follows;\n",
    "\n",
    "```\n",
    "url: https://wekeo-broker.apps.mercator.dpi.wekeo.eu/databroker\n",
    "user: <your_user_name>\n",
    "password: <your_password>\n",
    "```\n",
    "\n",
    "You must replace `<your_user_name>` and `<your_password>` with the information from your WEkEO account (if you don't have one yet, register [here](https://www.wekeo.eu/). Once you have entered these credentials in the file, the `hda` client will automatically read in the credentials from the file when you use it.\n",
    "\n",
    "To begin, we should establish an instance of the client."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = Client()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The WEkEO HDA client accepts requests as JSON queries. These have a specific format, which may look complex, but you can build on the examples you can find in the GUI, available under the **Show API request** button;\n",
    "\n",
    "<img src='../img/WEkEO_show_api_OLCI.png' align='centre' width='75%'></img>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "WEkEO allows you to search for data in time and space, but not by product name. However, we can work around this by providing the exact time stamps for our data. Below, we have a query for the first item in our product list, the OLCI level 1B product. You can see that the start and end times match the filename times, and the `producttype`, and `timeliness` correspond to those in the earlier specified filename too."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query = {\n",
    "  \"datasetId\": \"EO:EUM:DAT:SENTINEL-3:OL_1_EFR___\",\n",
    "  \"dateRangeSelectValues\": [\n",
    "    {\n",
    "      \"name\": \"position\",\n",
    "      \"start\": \"2021-07-17T10:10:14.000Z\",\n",
    "      \"end\": \"2021-07-17T10:10:16.000Z\"\n",
    "    }\n",
    "  ],\n",
    "  \"stringChoiceValues\": [\n",
    "    {\n",
    "      \"name\": \"producttype\",\n",
    "      \"value\": \"OL_1_EFR___\"\n",
    "    },\n",
    "    {\n",
    "      \"name\": \"timeliness\",\n",
    "      \"value\": \"Non Time Critical\"\n",
    "    }\n",
    "  ]\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This next section of code prints the names of the files that have been found during the search..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "matches = c.search(query)\n",
    "for match in matches.results:\n",
    "    fdst = match['filename']\n",
    "    print(f\"Found: {fdst}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that we have found the file we are interested in, so the next section of code can be used to download it..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "matches.download()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we can unzip the file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with zipfile.ZipFile(fdst, 'r') as zip_ref:\n",
    "    zip_ref.extractall(download_dir)\n",
    "    print(f'Unzipping of product {fdst} finished.')\n",
    "os.remove(fdst)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now lets download our other two products. We will need to set up query files for both."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "query_WRR = {\n",
    "  \"datasetId\": \"EO:EUM:DAT:SENTINEL-3:OL_2_WRR___\",\n",
    "  \"dateRangeSelectValues\": [{\"name\": \"position\", \"start\": \"2021-07-17T09:57:31.000Z\", \"end\": \"2021-07-17T09:57:33.000Z\"}],\n",
    "  \"stringChoiceValues\": [{\"name\": \"producttype\", \"value\": \"OL_2_WRR___\"}, {\"name\": \"timeliness\", \"value\": \"Non Time Critical\"}]\n",
    "}\n",
    "\n",
    "query_WFR = {\n",
    "  \"datasetId\": \"EO:EUM:DAT:SENTINEL-3:OL_2_WFR___\",\n",
    "  \"dateRangeSelectValues\": [{\"name\": \"position\", \"start\": \"2021-07-17T10:10:14.000Z\", \"end\": \"2021-07-17T10:10:16.000Z\"}],\n",
    "  \"stringChoiceValues\": [{\"name\": \"producttype\", \"value\": \"OL_2_WFR___\"}, {\"name\": \"timeliness\", \"value\": \"Non Time Critical\"}]\n",
    "}\n",
    "\n",
    "queries = [query_WRR, query_WFR]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And again we can search for, and download the results..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for query in queries:\n",
    "    matches = c.search(query)\n",
    "    matches.download()\n",
    "    for match in matches.results:\n",
    "        fdst = match['filename']\n",
    "        print(f\"Found: {fdst}\")        \n",
    "        with zipfile.ZipFile(fdst, 'r') as zip_ref:\n",
    "            zip_ref.extractall(download_dir)\n",
    "            print(f'Unzipping of product {fdst} finished.')\n",
    "        os.remove(fdst)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can now continue to work through the rest of the notebooks in this repository and learn more about working with OLCI data. Alternatively, if you'd like to learn more about using the advanced functionality of the WEkEO HDA API to access OLCI data you can check out the advanced workflows in the <a href=\"../2_OLCI_advanced/2_2_OLCI_advanced_data_access_hda.ipynb\">Advanced data access with the WEkEO **HDA**</a> notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>\n",
    "<a href=\"../Index.ipynb\"><< Index</a>\n",
    "<br>\n",
    "<a href=\"./1_2_OLCI_file_structure.ipynb\">Understanding OLCI product structure >></a>\n",
    "\n",
    "<hr>\n",
    "<a href=\"https://gitlab.eumetsat.int/eumetlab/ocean\">View on GitLab</a> | <a href=\"https://training.eumetsat.int/\">EUMETSAT Training</a> | <a href=mailto:ops@eumetsat.int>Contact helpdesk for support </a> | <a href=mailto:Copernicus.training@eumetsat.int>Contact our training team to collaborate on and reuse this material</a></span></p>"
   ]
  }
 ],
 "metadata": {
  "description": "This Jupyter Notebook covers how to access OLCI data via the EUMETSAT Data Store using the EUMDAC client, or via the WEKEO catalogue using the HDA adaptor.",
  "image": "",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  },
  "tags": {
   "category": "data_discovery",
   "domain": "ocean",
   "level": [
    "L1",
    "L2"
   ],
   "satellite": "Sentinel-3",
   "sensor": "OLCI",
   "variable": [
    "radiance",
    "reflectance",
    "ocean_colour"
   ]
  },
  "title": "OLCI data access"
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
